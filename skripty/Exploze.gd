extends Node2D

# uplně jednoduchej výbuch

var efekt
var casovac

# konstruktor s argumentama pozicí a barvou výbuchu
func _init(pozice,barva):
	
	self.position=pozice
	self.z_index=16
	
	self.efekt = Particles2D.new()
	
	efekt.amount=64
	efekt.lifetime=0.3
	
	# efekt se nám pustí jenom jednou
	# vyhodí těch 64 particlů a další generovat nebude
	efekt.one_shot=true
	
	# obrázek jednotlvejch částic. nemůžem použít atlas
	efekt.texture=preload('res://obrazky/castice.png')
	
	var material=ParticlesMaterial.new()
	
	# částečky budem vypouštět ve směru nahoru z kruhu o poloměru 32 pixelů
	# materiál má defautně zapnutou gravitaci 9.8g
	material.emission_shape=ParticlesMaterial.EMISSION_SHAPE_SPHERE
	material.emission_sphere_radius=32.0
	material.direction=Vector3(0.0,-1.0,0.0)
	
	#rozptyl v úhlu vypuštěnejch particlů
	material.spread=90.0
	
	material.initial_velocity=128.0
	material.color=barva
	
	efekt.process_material=material
	
	casovac = Timer.new()
	
	self.add_child(efekt)
	self.add_child(casovac)
	
func _ready():
	# začnem vypouštět
	efekt.emitting=true
	
	# v godotu nemá particle žádnou metodu kterou bysme jako zistili jestli efekt
	# má ještě nějaký živý particly a je načase voběkt vodstranit ze scény
	# prostě počkáme dobu delší než je doba života těch partilců takže budem mit
	# jistotu že už žádný particly nejsou a mužem voběkt dát do pryč ;D
	casovac.start(5)
	
func _process(delta):
	# nemusíme jenom čekat na signály mužem koukat na zbejvající čas časovače ručně
	if casovac.time_left<1:
		queue_free()
