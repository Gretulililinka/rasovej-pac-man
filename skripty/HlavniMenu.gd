extends Control

# takže co tady jako budeme dělat :O :O
# vyrobíme si jednoduchý meníčko se třema mačkátkama
# jedno mačkátko začne novou hru druhý mačátko ukáže tabulku nejvyších skóre s ménama
# noa poslední mačkátko ukáže credits

#abyto bylo takový zajímavější tak tam strčíme obrázek pacmana kterej se jakoby přikulí ze 
# starny doprostředka vobrazovky a až se přikulí tak z nebe jakoby spadne obrázek titulku tý
# naší hry
# noa abyto nebylo takový statický tak se pacman jakoby začne houpat na místě

var rozliseni

var pacman=null
var naklony=[20,-20]

var tweenNaklaneni=null
var tweenSestupuLoga=null

# jak který ty kroky našeho 'inra' budou jako trvat dlouho
const DOBA_ANIMACE_PRIKULENI = 1.0
const DOBA_SESTUPU_LOGA = 0.5

# si myslim že je nohem praktičtější gělat gui přímo v tom jejich editůrku ale aby jako
# bylo copypastovatelný tak semho udělala ve skriptu
func _ready():
	
	# uplně nazačátku hry vynulujem počítadlo skóre
	Skore.skore = 0
	
	# načtem a zapnem muziku
	var hudba = AudioStreamPlayer.new()
	hudba.stream=preload('res://zvuky/hudbaMenu.ogg')
	self.add_child(hudba)
	hudba.play()
	
	rozliseni=get_viewport_rect().size
	
	#načtem *.ttf font
	var font_titulku = DynamicFont.new()
	font_titulku.font_data = load('res://fonty/font.ttf')
	font_titulku.size = 48
	font_titulku.outline_size=4
	font_titulku.outline_color=Color(0,0,0,1)
	
	# nejdřiv na scénu strčíme obrázek pozadí by se jako vykresloval jako první a teda 
	# jako i uplně vespod pod jinejma věcma
	var obrazek=Sprite.new()
	obrazek.texture=preload('res://obrazky/main_menu.png')
	var sirka = 1344
	var vyska = 1792
	var velikostObrazku=obrazek.texture.get_size()
	obrazek.centered=false
	obrazek.set_scale(Vector2(sirka/velikostObrazku.x, vyska/velikostObrazku.y))
	self.add_child(obrazek)
	
	# všecky prvky gui budem strkat do kontejnéru kterej nám je bude v sobě hezky umisťovat a 
	# zvěčovat na požadovaný rozměry
	# použijem vertikální box abynám to tlačítka jakoby rovnalo nad sebe
	var kontejner=VBoxContainer.new()
	kontejner.rect_min_size=rozliseni-Vector2(64,rozliseni.y/2)
	kontejner.rect_size=rozliseni-Vector2(64,rozliseni.y/2)
	kontejner.rect_position.x+=32
	kontejner.rect_position.y=rozliseni.y/2
	
	self.add_child(kontejner)
	
	# vyrobíme tlačítka
	var tlacitko_hraj=Button.new()
	var tlacitko_skore=Button.new()
	var tlacitko_credits=Button.new()
	# a nastrkáme je do kontejneru
	kontejner.add_child(tlacitko_hraj)
	kontejner.add_child(tlacitko_skore)
	kontejner.add_child(tlacitko_credits)
	
	for tlacitko in kontejner.get_children():
	
		# iksovej rozměr nemá smysl protože vbox kontík je roztáhne na max šířku kterou muže
		tlacitko.rect_min_size=Vector2(0,256)
	
		# každej z prvků menu má size_flags pro vertikální a horizontální rozměr
		# určujou jak se prvek rozztáhne a umístí v prostoru kontejneru kterej jakoby má k dispozici
		# nóó a flag SIZE_EXPAND řiká mačkátkům aby se jako vertikálně rozmístili co nejdál vod sebe 
		tlacitko.size_flags_vertical=Button.SIZE_EXPAND
		tlacitko.set('custom_fonts/font', font_titulku)
		tlacitko.add_color_override('font_color', Color(1,0,0,1))


	# a dáme dlačítku hraj 'zaměření'
	# toje jakože ten prvek je aktivní. jeto stejný jako když někde na webu vyplňujete formulář 
	# tak taky bejvá aktivní právě jenom jediný vyplňovátko v tom formuláři
	# jeto jako bysme nato třeba klikli myší
	# my nechcem aby hráč dával ruce pryč z klávesnice a zkoumal co jako s tim
	# tak tam ten focus strčíme ručně
	# grab_focus() funguje jenom kdyžuž je node na scéně
	tlacitko_hraj.grab_focus()
	
	# dáme tlačítkům popisky a signálovitý callbacky
	tlacitko_hraj.text='Hraj'
	tlacitko_hraj.connect('pressed',self,'_on_TlacitkoHraj_pressed')
	tlacitko_skore.text='Nejvyší skóre'
	tlacitko_skore.connect('pressed',self,'_on_TlacitkoSkore_pressed')
	tlacitko_credits.text='Credits'
	tlacitko_credits.connect('pressed',self,'_on_TlacitkoCredits_pressed')
	
	# přidáme logo
	var logo=Sprite.new()
	logo.texture=load('res://obrazky/pacman_logo.png')
	logo.centered=false
	logo.position=Vector2(0,-500)
	self.add_child(logo)
	
	# a necháme ho spadnout
	tweenSestupuLoga=Tween.new()
	tweenSestupuLoga.interpolate_property ( logo, 'position', logo.position, Vector2(0,0), DOBA_SESTUPU_LOGA, Tween.TRANS_ELASTIC, Tween.EASE_IN_OUT)
	add_child(tweenSestupuLoga)
	
	# přidáme vobrázek velikýho pacmana
	pacman=Sprite.new()
	pacman.texture=load('res://obrazky/pacman_big.png')
	pacman.centered=true
	self.add_child(pacman)
	
	# s velikakakánským pacmanem budou pracovat hnedka tři tweeny
	# tween naklánění kdy se pacman jakoby bude houpat zestrany nastranu
	tweenNaklaneni=Tween.new()
	add_child(tweenNaklaneni)
	tweenNaklaneni.connect('tween_completed',self,'_on_tweenNaklaneni_completed')
	
	# tween posunu kdy pcman jakoby přijede zestrany doprostředka vobrazovky
	var tweenPosunu=Tween.new()
	add_child(tweenPosunu)
	tweenPosunu.interpolate_property ( pacman, 'position', Vector2(-128,rozliseni.y/2-138), Vector2(rozliseni.x/2,rozliseni.y/2-138), DOBA_ANIMACE_PRIKULENI, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tweenPosunu.start()
	# a tween točení abyse pacman jakoby při tom přesunu kutálel :D
	var tweenToceni=Tween.new()
	add_child(tweenToceni)
	tweenToceni.connect('tween_completed',self,'_on_tweenPosunu_completed')
	
	# rotaci voběktů mužem nastavovat radiánama ale i přímo vobyč úhlama ve stupních
	tweenToceni.interpolate_property ( pacman, 'rotation_degrees', -360, 0, DOBA_ANIMACE_PRIKULENI, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tweenToceni.start()

func _on_tweenPosunu_completed(objekt,klic):
	# posuneme počátek spritu dolu
	# toje bod středu rotace tý věci
	pacman.offset.y=-128
	# a vokolik sme počátek posunuli dolu vo tolik posunem voběkt nahoru
	# aby nám neklesnul dolu
	pacman.position.y+=128
	
	# noa rozhoupem ho a schodíme dolu logo
	tweenSestupuLoga.start()
	start_tweenNaklaneni()

func start_tweenNaklaneni():
	tweenNaklaneni.interpolate_property ( pacman, 'rotation_degrees', pacman.rotation_degrees, naklony[1], 1.0, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tweenNaklaneni.start()

func _on_tweenNaklaneni_completed(object, key):
	# vždycky když se pacman dohoupne tak se prohoděj ty úhly v náhlonech a zase se bude
	# naklánět na vopačnou stranu
	naklony.invert()
	start_tweenNaklaneni()
	
# nakonec callbacky těch tlačítek na přepínání scén ;D
func _on_TlacitkoHraj_pressed():
	get_tree().change_scene('res://sceny/Hra.tscn')
	
func _on_TlacitkoSkore_pressed():
	get_tree().change_scene('res://sceny/KonecHry.tscn')
	
func _on_TlacitkoCredits_pressed():
	get_tree().change_scene('res://sceny/Credits.tscn')
