extends Node2D

var zivotu=3
var sezranejch_bobku=0
var level=0

#za každýho sežranýho ducha ve stejným režimu vystrašení je násobně víc bodů než za předchozího
# tadleta proměná je počítadlo sežranejch duchů
var combo_sezranejch=0

# z duchovního domečku vypustíme ducha žvdycky když hráč sežere několikátej bobek
const KOLIK_BOBKU_NA_VYPUSTENI = 20
var sezranejch_bobku_na_vypusteni = 0

var hrac=null

var okraj=null
var bobky=null
var bludiste=null

var pozice_bobku=null

#políčko kde začíná hráč
const START = Vector2(10,20)

var panel=null
var duchove=[]
var duchove_doma=[]

#boužel nejde deklarovat víc proměnejch na jednom řádečku jakože var a=0,b=0,c=0 
# musí se každá vzlášť :O :'(
var cervenejDuch=null
var azurovejDuch=null
var oranzovejDuch=null
var ruzovejDuch=null

# na startu se občas oběvuje mizící sebratelnej bonus
# muže to bejt novej iphone krabice s botama nike nová televize nebo molotovovej koktejl
# za první tři sou extra body nóó a molotova se duchové děsně bojej 
var casovacBonusu=null
var bonus=null
var druh_bonusu=Bonus.IPHONE

# +- v rozích herní mapky sou umístěný čtyry molotovy který když se dostanou pacmanoj do rukou 
# tak dostanou duchové pěknej strach :O ;D
var molotovy=[null,null,null,null]
const SOURADNICE_MOLOTOVU=[Vector2(1,20),Vector2(19,20),Vector2(1,3),Vector2(19,3)]

var casovacVystraseni=null
const DOBA_VYSTRASENI=10

var casovacBlikani=null

const KDY_ZACNE_BLIKAT=DOBA_VYSTRASENI * 0.9

var casovacRezimu=null
var kolikata_zmena_rezimu=0
var trvani_rezimu_rozptyleni=20
var trvani_rezimu_pronasledovani=20

#časovač vypouštění duchů
# vždycky koukne jestli sou nějaký sežraný duchové vrácený domu a kdyžtak jednoho pusí ven
#var casovac_vypousteni=null
#const DOBA_VYPOUSTENI=30

# časovač restartu hry po tom co byl hráč zakleknutej některým duchem
# nemůžeme hru zastavit a zapnout znova hnedka musíme nechat hráče pochopit cose jako stalo
var casovacZakleknuti=null
const DOBA_ZAKLEKNUTI=2.0

# sem si budeme skovávat instance požárů abysme je mohli rychle uhasit když restartujem level
var pozary = []

var generator_nahodnych_cisel

# načtem jednotlivý skripty/třídy by sme jako mohli dělat instance těch oběktů

const HRAC_SKRIPT = preload('res://skripty/Hrac.gd')

const AZUROVEJ_DUCH_SKRIPT = preload('res://skripty/AzurovejDuch.gd')
const CERVENEJ_DUCH_SKRIPT = preload('res://skripty/CervenejDuch.gd')
const ORANZOVEJ_DUCH_SKRIPT = preload('res://skripty/OranzovejDuch.gd')
const RUZOVEJ_DUCH_SKRIPT = preload('res://skripty/RuzovejDuch.gd')

const PANEL_SKRIPT = preload('res://skripty/Panel.gd')
const BONUS_SKRIPT = preload('res://skripty/Bonus.gd')
const POZAR_SKRIPT = preload('res://skripty/Pozar.gd')
const ZACATEK_KOLA_SKRIPT = preload('res://skripty/ZacatekKolaText.gd')


#zvuky
var zvukBobek=null
var zvukBonus=null
var zvukSezraniDucha=null
var zvukZakleknuti=null

#hudba
var hudbaNormalni = null
var hudbaAkcni = null

#strčí duchy a hráče zpátky na start a jakoby znovu nastaví jejich stavy
func start_figurek(bludiste):
	
	# červenej duch začíná přededveřma duchovního domečku a de hnedka doleva
	cervenejDuch.nastav(okraj,hrac,Duch.POZICE_PRED_DOMECKEM,Duch.RezimPohybu.VYPOUSTENI)
	
	cervenejDuch.vypustit_ducha()
	# růžovej duch začíná v domečku ale hnedka ho vopouští
	ruzovejDuch.nastav(okraj,hrac,Duch.DOMECEK_POZICE,Duch.RezimPohybu.VYPOUSTENI)
	
	# voranžovej a azurovej duch zatim nervózne přešlapujou doma a čekaj až pacman sežere dost bobků
	# a tim budou puštěný ven
	oranzovejDuch.nastav(okraj,hrac,Duch.DOMECEK_POZICE-Vector2(1,0),Duch.RezimPohybu.JE_DOMA)
	azurovejDuch.nastav(okraj,hrac,Duch.DOMECEK_POZICE+Vector2(1,0),Duch.RezimPohybu.JE_DOMA)
	
	#azurovej duch při pronásledování prostě jako musí vědět kde je červenej duch tak si drží jeho referenci
	azurovejDuch.cervenejDuch=cervenejDuch 
	
	#růžovej duch začíná v domečku ale jako právě vypuštěnej ven :O :O
	ruzovejDuch.vypustit_ducha()
	bludiste.otevri_dvere()
	
	#naši duši si vždycky vybíraj novej krok v 'callbacku' kterej je volanej při ukončení kroku
	#aby se jako začali hejbat musíme do nich jakože drcnout a ručně zavolat ten callback
	#argumenty callbacku maj smysl když je volanej z rodičovský třídy Ducha Figurky tady nemaj smysl a
	#je bezpečný nam nastrkat null
	for duch in duchove:
		duch._on_TweenKroku_tween_completed(null,null)
		duch.nastav_rezim_rozptyleni()
		
	#v pravidlech je že když se přepne režim duchů z rozptýlenýho nebo pronásledovacího
	# tak vokamžitě měnej směr na vopačnej než šli
	# my ale uplně nazačátku chceme aby nám červenej duch pokračoval furt doleva (rudý a levá tode dohromady ;D)
	# a současně chceme začít v rozptylovacím režimu pohybu. 
	# prostě červenýmu duchoj přepnem tamtu proměnou co dělá že se v příštím kroku otočí a jeto :O ;D
	# cervenejDuch.otoc_se=false
	
	hrac.nastav(okraj,bludiste.get_node('Bobky'),START)
	
	#dáme azurovýho a voranžovýho ducha dodomečku
	duchove_doma = [ azurovejDuch, oranzovejDuch ]

# restart různejch časovačů co tam jako máme
func spustit_casovace():
	casovacBonusu.start(30)
	casovacRezimu.paused=false #metoda start nepřepne pauznutej stav musí se ručně
	casovacRezimu.start(trvani_rezimu_rozptyleni)
	kolikata_zmena_rezimu=0
	
	#muže nám jet blikání a vystrašení takže je jako zastavíme i když třeba zrovna teďko nejedou
	casovacVystraseni.stop()
	casovacBlikani.stop()
	
# sou tady dvě podobný funkce 'restartuj level' a 'nový level'
# restart vrátí figurky duchů a hráče nazačátek
# noa novej level udělá level uplně vod začátku jako včetně tohodletoho vracení ale
# taky vobnoví rozmístění bobků a malotovů na mapě a tak
# restart se nevolá jenom nazačátku levelu ale taky když hráč umře aje vrácenej na start

func restartuj_level():
	# zastavíme možný právě probíhající tweeny kroků u všech duchů aby nám jejich metoda
	# _on_TweenKroku_tween_completed nepokazila nastavený stavy a pozice
	for duch in duchove:
		duch.stuj()
		duch.normalni_zhled()
	
	# nastartu muže ležet bonus tak ho skováme
	bonus.visible=false
	# a restartujem jeho druch
	druh_bonusu=Bonus.IPHONE
	
	# vynulujem počítadlo bobků pro vypouštění duchů
	sezranejch_bobku_na_vypusteni=0
	# vrátíme figurky do jejich počátečních stavů
	start_figurek(bludiste)
	# pustíme časovače od začátku
	spustit_casovace()
	
	#zavoláme metodu hráče která ho vrací do výhozího stavu
	hrac.oziv()
	
	var priprava_text=ZACATEK_KOLA_SKRIPT.new()
	self.add_child(priprava_text)
	
	hudbaAkcni.stop()
	hudbaNormalni.play()
	
	bludiste.nastav_praporek_usa()
	
	# TODO: tenhleten kousek kódu dělal že se to někdy seklo :o :o
	# patrně souvisí s bugem https://github.com/godotengine/godot/issues/32383
	# vopraveno bude prej až v 4.0 godotoj :/ :/
	# uhasíme možný požáry
	"""
	for pozar in pozary:
		# kouken jestli je instance požáru validní
		# jakože neni null nebo už připravená na vodstranění
		if is_instance_valid(pozar):
			pozar.queue_free()
	pozary = []
	"""
	
func novy_level():
	
	restartuj_level()
	
	# obnovíme rozmístění bobků v bludišti
	for pozice in pozice_bobku:
		bobky.set_cellv(pozice,0)
		
	# sežranejch bobků má pacman zase nuličku :O :'(
	# bude je muset spapat všecky uplně znova :O :O
	sezranejch_bobku=0

	# znova uděláme molotovy sebratelný
	for molotov in molotovy:
		molotov.visible=true
		
	level+=1
	# s každým levelem bude rozptylovací režim duchů kratčí a prodásledovací víc delší
	trvani_rezimu_pronasledovani = 18 + (level * 2)
	trvani_rezimu_rozptyleni = 10 + (10 / level)
	panel.nastav_level(level)
	
func nacti_zvuky():
	# načtem hudby a zvuky
	
	# používám load namísto preload
	# load normálně načte nějakou věc za běhu nóó a 
	# preload jakoby za času kompilace takže seto jakoby dovopravdicky neloaduje
	# ale někte toje v tý binárce jako už načtený předem
	zvukBobek=AudioStreamPlayer.new()
	zvukBobek.stream=load('res://zvuky/bobek.wav')
	self.add_child(zvukBobek)
	
	zvukBonus=AudioStreamPlayer.new()
	zvukBonus.stream=load('res://zvuky/bonus.wav')
	self.add_child(zvukBonus)
	
	zvukSezraniDucha=AudioStreamPlayer.new()
	zvukSezraniDucha.stream=load('res://zvuky/sezraniDucha.wav')
	self.add_child(zvukSezraniDucha)
	
	zvukZakleknuti=AudioStreamPlayer.new()
	zvukZakleknuti.stream=load('res://zvuky/zakleknuti.wav')
	self.add_child(zvukZakleknuti)
	
	hudbaNormalni=AudioStreamPlayer.new()
	hudbaNormalni.stream=load('res://zvuky/hudbaNormalni.ogg')
	self.add_child(hudbaNormalni)
	
	hudbaAkcni=AudioStreamPlayer.new()
	hudbaAkcni.stream=load('res://zvuky/hudbaAkcni.ogg')
	self.add_child(hudbaAkcni)

func _ready():
	
	# vyrobíme si generátorek náhodnejch čísel a ziniciealizujeme ho
	generator_nahodnych_cisel = RandomNumberGenerator.new()
	generator_nahodnych_cisel.randomize()
	
	nacti_zvuky()
	
	panel=PANEL_SKRIPT.new()
	self.add_child(panel)
	
	# vyrobíme časovače a napojíme jejich signály a callbacky
	casovacBlikani=Timer.new()
	self.add_child(casovacBlikani)
	casovacBlikani.connect('timeout',self,'_on_CasovacBlikani_timeout')
	
	#tamten parametr one_shot řiká že časovač se pustí jenom jednou
	#výchozí hodnota je false a znamená to že se časovač pouští dokolečka pořád znova
	casovacBlikani.one_shot=true
	
	casovacVystraseni=Timer.new()
	self.add_child(casovacVystraseni)
	casovacVystraseni.connect('timeout',self,'_on_CasovacVystraseni_timeout')
	casovacVystraseni.one_shot=true
	
	casovacRezimu=Timer.new()
	self.add_child(casovacRezimu)
	casovacRezimu.connect('timeout',self,'_on_CasovacRezimu_timeout')
	casovacRezimu.one_shot=true
	
	casovacBonusu=Timer.new()
	self.add_child(casovacBonusu)
	casovacBonusu.connect('timeout',self,'_on_CasovacBonusu_timeout')
	
	casovacZakleknuti=Timer.new()
	self.add_child(casovacZakleknuti)
	casovacZakleknuti.connect('timeout',self,'_on_CasovacZakleknuti_timeout')
	casovacZakleknuti.one_shot=true
	
	# načtem scénu bludište a umístime ji jako instanci do stromu
	var scene = load('res://sceny/Bludiste.tscn')
	bludiste=scene.instance()
	
	# posuneme bludiště o vejšku panelu dolu
	bludiste.position.y=64
	
	# budem potřebovat dlaždicovou mapu okraje
	okraj=bludiste.get_node('DlazdicovaMapa')
	# a dlaždicovou mapu s bobkama bavlny
	bobky=bludiste.get_node('Bobky')
	self.add_child(bludiste)
	
	# zapamatujeme si výchozí rozmístění všech bobků
	# vždycky když bude dohranej level je potřeba bobky zase vobnovit
	# a je škoda kuli tomu načítat celou bobkovou scénku znova :O ;D
	pozice_bobku=bobky.get_used_cells()
	
	# vyrobíme zapojíme a přidáme herní figurky ;D
	
	hrac=HRAC_SKRIPT.new()
	bludiste.add_child(hrac)
	hrac.napojit_signaly(self)
	
	cervenejDuch=CERVENEJ_DUCH_SKRIPT.new()
	duchove.append(cervenejDuch)
	
	oranzovejDuch=ORANZOVEJ_DUCH_SKRIPT.new()
	duchove.append(oranzovejDuch)
	
	ruzovejDuch=RUZOVEJ_DUCH_SKRIPT.new()
	duchove.append(ruzovejDuch)
	
	azurovejDuch=AZUROVEJ_DUCH_SKRIPT.new()
	duchove.append(azurovejDuch)
	
	for duch in duchove:
		bludiste.add_child(duch)
		duch.napojit_signaly(self)
	
	# v bludišti se vobčas udělá na startu dočasnej mizicí bonus
	bonus=BONUS_SKRIPT.new(druh_bonusu,START,hrac)
	bonus.napojit_signaly(self)
	bludiste.add_child(bonus)
	
	# v bludišti sou na čtyrech místech trvalý nemizicí molotovy koktejly
	for i in range(4):
		var molotov=BONUS_SKRIPT.new(Bonus.MOLOTOV,SOURADNICE_MOLOTOVU[i],hrac)
		molotov.napojit_signaly(self)
		bludiste.add_child(molotov)
		molotovy[i] = molotov
	
	novy_level()
	
	
	for _i in range(zivotu):
		panel.pridej_zivot()
	
# signál posílanej z instance hráče ždycky když sežere bavlněnej bobek
func _on_Hrac_sezral_bobek():
	sezranejch_bobku+=1
	Skore.skore+=10
	panel.nastav_skore(Skore.skore)
	zvukBobek.play()
	
	# v bludišti je maximálně 188 bobků
	# když je pacman všecky sní tak vyhrál a začne novej level
	if sezranejch_bobku == 188:
			novy_level()
			return
	
	# pokud je doma skovanej nějakej duch tak započítáme bobek na jeho vypuštění
	if len(duchove_doma) > 0:
		sezranejch_bobku_na_vypusteni += 1
	# noa jestli máme dost vypouštěcích bobků tak vypustíme ducha a počítadlo restartujem
	# ducha vypouštíme jenom když neni režim vyděšení. to poznáme podle toho jestli je timer měnění 
	# režimu pauznutej nebo ne
	if sezranejch_bobku_na_vypusteni >= KOLIK_BOBKU_NA_VYPUSTENI and not casovacRezimu.paused:
		
		#pop_front odstraní z pole první prvek a vrátí nám ho 
		#pokud je pole prázdný vrací null
		var duch = duchove_doma.pop_front()
		if duch != null:
			duch.vypustit_ducha()
			bludiste.otevri_dvere()
			sezranejch_bobku_na_vypusteni -= KOLIK_BOBKU_NA_VYPUSTENI
		
# chytá signál posílanej duchem když se mu podaří zakleknout pacmana
# konec levelu neni vokamžitej ale až v timeoutu časovače zakleknutí
# máto ten smysl abysme nejdřiv nechali doběhnout animaci zakleknutýho pacmana
func _on_Duch_zakleknuti():
	zvukZakleknuti.play()
	hrac.zaklekni()
	zivotu-=1
	panel.odeber_zivot()
	casovacZakleknuti.start(DOBA_ZAKLEKNUTI)
	
# chytá signál z bonusu když ho hráč sebere a bonus chce přidat body
# teoreticky bysme tohleto mohli dělat rovnou z bonusu mě tak napadá ale couž :O :/
func _on_Bonus_pridej_body(bodu):
	Skore.skore+=bodu
	panel.nastav_skore(Skore.skore)
	zvukBonus.play()

# se zavolá když se pacmanoj dostane do rukou molotov
func _on_Bonus_molotov():
	
	#uděláme nějakej zvukovej efekt
	# mužete si pro to najít nějakej zvuk extra já sem děsně eko jak jako víte no tak sem 
	# zreciklovala už jednou použitej zvuk sežrání ducha :D ;D
	zvukSezraniDucha.play()
	
	# jestli ještě nehraje akční hudba tak ji na akční přepnem
	# zastavíme normální a pustíme tu akční :D
	if not hudbaAkcni.playing:
		hudbaNormalni.stop()
		hudbaAkcni.play()
		
	# uděláme duchy vyděšenejma
	for duch in duchove:
		duch.nastav_rezim_vydeseni()
		
	# přepnem hráče do akčního režimu
	hrac.akcni_rezim()
	
	# zapneme blikání pacmaního praporku misto tamtoho staroamerickýho
	bludiste.nastav_praporek_blm()
	
	# když pacman sebere molotova tak to tam všecko kolem sebe jakoby trošičku podpálí :O :O :D :D ;D
	# zkusíme podpálit čtvereček 11x11 políček kolem hráče s tim že budem podpalovat jakoby
	# jenom plíčka kde je kostička okraje bludiště a vždycky si navíc hodíme korunou jestli totam
	# podpálíme aby to jako vypadalo přirozenějc :O ;D
	for x in range(-5,5):
		for y in range(-5,5):
			var logicka_souradnice_pozaru=Vector2(x,y)+hrac.logicka_pozice
			
			if not okraj.get_cellv(logicka_souradnice_pozaru) == TileMap.INVALID_CELL:
				
				#vygenerujem si náhodný číslo od nuly do jedný a kouken jestli je menší než 0.5
				# toje skoro jako házet korunou jakože 50% šance
				if generator_nahodnych_cisel.randf() < 0.5:
					var pozar=POZAR_SKRIPT.new()
					# přepočítáme logickou souřadnici požáru na souřadnici v mapě přidáme (32,32) 
					# aby jako požár byl vycentrovanej uprostřed kostičky naší herní mapy
					# dobu trvání dáme náhodně mezi pěti a deseti sekundama (uvnitř ve třídě plus jednu za dobu vypínání)
					pozar.nastav(logicka_souradnice_pozaru*64 + Vector2(32,32),generator_nahodnych_cisel.randf_range(4.0,8.0))
					bludiste.add_child(pozar)
					#pozary.append(pozar)
			
		
	# vypneme časovač kterej mění rozptylovací a pronásledovací režim
	casovacRezimu.paused=true
	# a pustíme vodpočet zkončení toho režimu vystrašení
	# a taky začátek blikání který jako hráčoj řiká žeto vystrašení brzy skončí ;D
	casovacVystraseni.start(DOBA_VYSTRASENI)
	casovacBlikani.start(KDY_ZACNE_BLIKAT)
	
# chytá signál posílanej z ducha když je sežranej
func _on_Duch_sezranej():
	zvukSezraniDucha.play()
	# je tady bonus za kombo sežranejch duchů
	# za prvního 200, za druhýho 400, pak 800 a 1600
	Skore.skore+=200 * pow(2,combo_sezranejch) 
	panel.nastav_skore(Skore.skore)
	combo_sezranejch+=1
	
# tendleten callback je volanej ždycky když duch orazí domu
# signál posílá informaci kterejže duch to právě jako dorazil domu
func _on_Duch_doma(ktery):
	duchove_doma.append(ktery)
	zvukSezraniDucha.play()
	
# chytá signál z časovače měnění režimu
func _on_CasovacRezimu_timeout():
	kolikata_zmena_rezimu+=1
	#pokud je změna režimu lichá, přepnem duchy do režimu rozptýlení
	if kolikata_zmena_rezimu % 2 == 0:
		for duch in duchove:
			duch.nastav_rezim_rozptyleni()
		casovacRezimu.start(trvani_rezimu_rozptyleni)
	#pokud sudá do pronásledování
	else:
		for duch in duchove:
			duch.nastav_rezim_pronasledovani()
		casovacRezimu.start(trvani_rezimu_pronasledovani)
		# každý příští přepnutí do pronásledování bude o počet levelů *2 sekund delší aby
		# jakoby rostla vobtížnost
		#v originále jeto složitější
		trvani_rezimu_pronasledovani += level * 2
	
func _on_CasovacBonusu_timeout():
	# pokud bonus neexistuje tak ho vyrobíme a dáme na start noa jestli jako existuje
	# tak ho jakoby zničíme
	if not bonus.visible:
		
		#pokud je hráč nastartu když tam chceme šoupnout bonus nóó tak prostě 
		#vteřinku počkáme a zkusíme to znova. jinak by nám to rovnou sebral ten bonus
		if hrac.logicka_pozice == START:
			casovacBonusu.start(1)
			return
		
		bonus.nastav_druh(druh_bonusu)
		bonus.visible=true
		
		if druh_bonusu<Bonus.MOLOTOV:
			druh_bonusu+=1
		else:
			druh_bonusu=Bonus.IPHONE
		
		casovacBonusu.start(15)
		
	else:
		bonus.visible=false
		casovacBonusu.start(45)
		
# skončení režimu vystrašení
func _on_CasovacVystraseni_timeout():
	# zase zapnem přepínací časovač režimu a vynulujem kombo sežranejch duchů
	casovacRezimu.paused=false
	combo_sezranejch=0
	
	# pustíme normální muziku
	hudbaNormalni.play()
	hudbaAkcni.stop()
	
	# hráče přepnem do normálního režimu
	hrac.normalni_rezim()
	# uprostřed bludiště zase aktivujem staroamerickej praporek
	bludiste.nastav_praporek_usa()
	
	for duch in duchove:
		duch.ukonci_rezim_vydeseni()

# dává nám vědět se vyděšenej režim končí :O :O
func _on_CasovacBlikani_timeout():
	for duch in duchove:
		duch.rozblikat()
		
# řasovač zakleknutí
# hráč umřel nóó a mi buťto restartujem hru a nebo zkončíme uplně
func _on_CasovacZakleknuti_timeout():
	
	if zivotu>0:
		restartuj_level()
	else:
		# jestli hráč už nemá životy tak přepnem celičkej stromeček scény na scénu 
		# s highscore
		get_tree().change_scene('res://sceny/KonecHry.tscn')
	
