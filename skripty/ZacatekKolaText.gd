extends Node2D

# tohle je takový to logo co se zobrazuje před začátkem kola hry
# v pacmanoj má vietnamec nápis 'ready' já použila znova to logo z hlavního menu

# jakmile tenhleten voběkt vstoupí do stromečku scény tak na chvilku pauzne hru zahraje
# znělka a logo spadne z vobrazovky a hra se zase zapne 

var zvukVyhra = null
var obrazek = null

func _ready():
	
	# strčíme doprostředka vobrazovky
	self.position=get_viewport_rect().size / 2
	
	# načtem a zahrajem znělku
	zvukVyhra=AudioStreamPlayer.new()
	zvukVyhra.stream=load('res://zvuky/vyhra.wav')
	# budem chytat signal dohrání znělky
	zvukVyhra.connect('finished',self,'_on_znelka_dohrala')
	self.add_child(zvukVyhra)
	zvukVyhra.play()
	
	# přidáme vobrázek loga
	obrazek=Sprite.new()
	obrazek.texture=preload('res://obrazky/pacman_logo.png')
	self.add_child(obrazek)
	
	# noa pauznem hru
	# potřebujem pauznout všecko nóó ale taky potřebujem
	# aby se nám ta hra taky nějak vopauzla protože pauznutý nody prostě nic nedělaj 
	# timery signály _process nic z toho nejdede :O :O
	# naštstí uzlům mužeme nastavovat atribut pasue_mode a mužem jim na pauzu udělat vyjímku
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	
	# pause mode u tohodle voběktu předělanej a mužem pauznout celej stromeček scény
	self.get_tree().paused = true
	
# metoda co se zavolá až znělka jakoby dohraje
func _on_znelka_dohrala():
	# vodpauzujem celej strom
	self.get_tree().paused = false
	
	# uděláme si tween na schození toho loga z vobrazovky
	var tween_spadnuti=Tween.new()
	self.add_child(tween_spadnuti)
	
	tween_spadnuti.connect('tween_completed',self,'_on_tween_dojel')
	# tween použujem na vobrázek budem měnit jeho pozici na pozici vo hodně moc víc dole během sekundy
	tween_spadnuti.interpolate_property ( obrazek, 'position', obrazek.position, obrazek.position+ Vector2(0,2048), 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween_spadnuti.start()

# tween dojel všecko hotový už mužem oběkt akorát tak zničit a zahodit :D ;D
func _on_tween_dojel(obekt, klic):
	self.queue_free()
