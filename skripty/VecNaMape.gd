extends Node2D

# prostě něco co leží na dlažicový mapě
# všechny uzly maj nějakou svou pozici relativní k nadřízenýmu uzlíku
# pro věci co se budou pohybovat po herní dlaždicový mapě chceme znát i jejich
# logickou souřadnici, jakože na kterým políčku v tý tile mapě stojej co maj před sebou a tak 

class_name VecNaMape

# logická pozice tý věci. odpovídá pozici v tile mapě
var logicka_pozice = Vector2(0,0)

func _process(delta):
	vypocitejLogickouPozici()
	
	# oběkty maj atribut z_index kterej určue pořadí jejich vykreslování. čim je hodnota
	# vyší tim dřív bude node vykreslenej voproti vostatním se stejným rodičovským uzlem
	# tady nastavujem na ypsilonovou souradnici, čim bude oběk na vobrazovce níž, tim dřív bude 
	# jakoby namalovanej. takle uděláme uplně primitivní iluzi nějaký perspektivy :D ;D
	self.z_index = self.logicka_pozice.y
	
# funkce volaná každým krokem která aktualizujuje logickou pozici přepočtem z pozice uzlu
# logická pozice pak vodpovídá nějakýmu políčku v tilemapě
func vypocitejLogickouPozici():
	logicka_pozice.x = ceil((self.position.x)/64)-1
	logicka_pozice.y = ceil((self.position.y)/64)-1
	
