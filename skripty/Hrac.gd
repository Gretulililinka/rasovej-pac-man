extends 'res://skripty/Figurka.gd'
# hráč dědí z figurky

class_name Hrac

var akce=DOLU
var bobky=null
var zakleknuty=false

# vokrasnej particlovej efekt když pacman sebere molotov
# bude se za nim dělat taková jakože hnědá šmouha jakože děsně rychle utiká :D
var efekt = null
var material_efektu = null

# vobyčejná normální rychlost přehrávání animace a zrychlený přehrávání když sebere bonus
const NORMALNI_FPS = 28.0
const AKCNI_FPS = NORMALNI_FPS * 1.3

# hráč posílá do hlavní scény signál že sebral bavlněnej bobek
signal sezral_bobek

# napojíme signály k scéně
func napojit_signaly(koren):
	self.connect('sezral_bobek',koren,'_on_Hrac_sezral_bobek')

# nastavování atributů budem volat opakovaně tak nepoužijem konstruktor
func nastav(okraj,bobky,logicka_pozice):
	self.okraj=okraj
	self.bobky=bobky
	self.logicka_pozice=logicka_pozice
	self.position=logicka_pozice*64 + Vector2(32,32)
	zakleknuty=false

# vygenerování jednotlivejch animací akcí animací z atlasu
# kromě animací 'dolu' 'nahoru' 'prava' který by měli mit všecky věci dědicí z figurky
# je tady navíc animace 'umrel' která se bude přehrávat když bude pacman zakleknutej
# 
func vygeneruj_animace(fps):
	# vezmem seznam snímků animace
	var seznamAnimaci=animace.get_sprite_frames()
	# a pokuť neexistuje tak ho jakoby vyrobíme
	if seznamAnimaci==null:
		seznamAnimaci=SpriteFrames.new()
		
	seznamAnimaci.add_animation('dolu')
	seznamAnimaci.add_animation('nahoru')
	seznamAnimaci.add_animation('prava')
	seznamAnimaci.add_animation('umrel')
	
	var index=0
	for i in range(7):
		seznamAnimaci.add_frame('dolu',textura_z_atlasu_indexem(atlas,i+index))
	# a ještě vokonce zpátky k začátku aby nám to hezky navazovalo
	for i in range(1,6):
		seznamAnimaci.add_frame('dolu',textura_z_atlasu_indexem(atlas,(6-i)+index))
		
	index+=7
	for i in range(7):
		seznamAnimaci.add_frame('umrel',textura_z_atlasu_indexem(atlas,i+index))
		
	index+=7
	for i in range(7):
		seznamAnimaci.add_frame('prava',textura_z_atlasu_indexem(atlas,i+index))
	for i in range(1,6):
		seznamAnimaci.add_frame('prava',textura_z_atlasu_indexem(atlas,(6-i)+index))
		
	index+=7
	for i in range(7):
		seznamAnimaci.add_frame('nahoru',textura_z_atlasu_indexem(atlas,i+index))
	for i in range(1,6):
		seznamAnimaci.add_frame('nahoru',textura_z_atlasu_indexem(atlas,(6-i)+index))
	
	seznamAnimaci.set_animation_speed('dolu',fps)
	seznamAnimaci.set_animation_speed('prava',fps)
	seznamAnimaci.set_animation_speed('nahoru',fps)
	seznamAnimaci.set_animation_speed('umrel',fps/4)
	
	# vypnem animaci umření loopování aby nehrála znova vozačátku když zkončí
	seznamAnimaci.set_animation_loop('umrel',false)

	animace.set_sprite_frames(seznamAnimaci)

func _ready():
	doba_kroku=12.0 / NORMALNI_FPS
	atlas=preload('res://obrazky/hrac.png')
	sirka_atlasu=7
	vyska_obrazku=64
	vygeneruj_animace(NORMALNI_FPS)
	
	# nastavíme výchozí vobrázek spritu
	animace.play('dolu')
	animace.stop()
	
	# vyrobíme si particle efekt rychlího běhání pacmana
	material_efektu = ParticlesMaterial.new()
	
	# vypouštet je budem furt z jednoho jedinýho bodu
	material_efektu.emission_shape=ParticlesMaterial.EMISSION_SHAPE_POINT
	
	# výchozá směr. budem měnit každým krokem ale :O :O
	# musí to vypadat že jakoby vyletujou z něj vopačným směrem než zrovinka de
	material_efektu.direction=Vector3(0,-1,0)
	
	# rozptyl rychlost a zvěčení částiček efektu
	material_efektu.spread= 5.0
	material_efektu.initial_velocity=500
	material_efektu.scale = 5.0
	
	# tahleta barvička odpovídá barvičce kůže pacmana ;D ;D
	material_efektu.color = Color('#431811') 
	# blbý na html kódech barviček je že tam nejde strčit alpha a my potřebujem abyto bylo trošičku průsvitný
	material_efektu.color.a = 0.5
	
	
	efekt=Particles2D.new()
	efekt.amount=16
	efekt.lifetime = 0.125
	efekt.process_material=material_efektu
	efekt.texture=preload('res://obrazky/castice.png')
	
	efekt.emitting = false
	
	# efekt bude strčenej pod pacmanem aby ho částečky nepřekrejvali
	efekt.z_index = -1
	self.add_child(efekt)

func _process(delta):
	
	# pokuť je pacman zakleknutej tak už nděláme nic nemá to cenu :D ;D
	if zakleknuty:
		return
	
	# pokud se pacman nachází na logický souřadnici vodpovídající souřadnici bobku 
	# v bludišti tak ten bobek sebere ;D
	# že je v mapě na daný souřadnici zistíme metodou get_cell která nám vrátí index
	# druhu tý dlaždiče. nóóó a jestli žádná dlažička na tom místě neni tak vrátí INVALID_CELL
	if bobky.get_cell(logicka_pozice.x,logicka_pozice.y)!=TileMap.INVALID_CELL:
		
		# přepnem dlažičku na invalid. tim ji z mapy vymažem 
		bobky.set_cell(logicka_pozice.x,logicka_pozice.y,TileMap.INVALID_CELL)
		# a očlem signál do hlavní scény
		emit_signal('sezral_bobek')
	
	# vybereme požadovanej směr kterým se hráč chce vydat podle toho jakou šipku 
	# právě drží
	if Input.is_action_pressed('ui_down'):
		akce = DOLU
	elif Input.is_action_pressed('ui_up'):
		akce = NAHORU
	elif Input.is_action_pressed('ui_right'):
		akce = PRAVA
	elif Input.is_action_pressed('ui_left'):
		akce = LEVA
	else:
		# noa jestli nerží žádnou tak pacman bude chtít pokračovat se svým předchozím směru
		akce = predchozi_smer
	
	# pokud máme aktivní particle efekt upravíme jeho směr aby vodpovídal vopačným směru posledního
	# provedenýho krůčku
	if efekt.emitting:
		efekt.process_material.direction = Vector3(-predchozi_smer.x, -predchozi_smer.y, 0)
	
	# pokud zrovinka neprobíhá krok tweenu tak vybereme nějakou akci na základě toho
	# jakou šipku hráč drží, jakej byl předchozí směr a jestli máme před pacmanem překážku
	if !jde:
			
		# todle je skutečnej směr kterým pudem
		var smer = null
		
		#pacman jakoby furt teda chce pokračovat stejným směrem dál na další políčko
		#pokud před pacmanem neni překážka nastavíme nejdřiv jako potenciální směr pacmanův předchozí
		if okraj.get_cell(logicka_pozice.x+predchozi_smer.x,logicka_pozice.y+predchozi_smer.y)==-1:
			smer=predchozi_smer
		
		#pokud bylo zmáčknutý mačkátko s šipkou a políčko v tom směru je volný pacman pude tam
		if okraj.get_cell(logicka_pozice.x+akce.x,logicka_pozice.y+akce.y)==-1:
			#jediná vyjímka je zahejbání do domečku duchů tam hráč vlízt nemuže :O ;D
			#pokud stojí přededveřma do domečku nemuže zahnout dolu
			if akce==DOLU and logicka_pozice==Vector2(10,10): 
				pass
			#jinak se směr bere normálně ze šipek
			else:
				smer=akce
		
		#pokud byl určenej směr předchozíma podmínkama deme
		#jinak pacman zustane stát namístě :O ;D
		if smer!=null:
			jdi(smer)
		
# zakleknutí pacmana duchem
# zastavíme tween a zahrajem animaci umření
func zaklekni():
	self.animace.play('umrel')
	stuj()
	zakleknuty=true
	
# vrátí stavy pacmana do výchozího stavu
func oziv():
	stuj()
	animace.play('dolu')
	animace.stop()
	zakleknuty=false
	jde=false
	predchozi_smer=DOLU
	normalni_rezim()
	
# pacman má dva pomyslný režimy, akční a normální
# během akčního má zrychlený přehrávání animací a kračí domu kroku a vypouští 
# zadní stranou ty particly
func akcni_rezim():
	doba_kroku=12.0 / AKCNI_FPS
	
	var seznamAnimaci = animace.get_sprite_frames()
	seznamAnimaci.set_animation_speed('dolu',AKCNI_FPS)
	seznamAnimaci.set_animation_speed('prava',AKCNI_FPS)
	seznamAnimaci.set_animation_speed('nahoru',AKCNI_FPS)
	
	self.efekt.emitting = true
	
# noa navopak normální režim kdy chodí pomalejc a nic ze sebe nevypouští ;D ;D
func normalni_rezim():
	doba_kroku=12.0 / NORMALNI_FPS
	
	var seznamAnimaci = animace.get_sprite_frames()
	seznamAnimaci.set_animation_speed('dolu',NORMALNI_FPS)
	seznamAnimaci.set_animation_speed('prava',NORMALNI_FPS)
	seznamAnimaci.set_animation_speed('nahoru',NORMALNI_FPS)
	
	self.efekt.emitting = false
