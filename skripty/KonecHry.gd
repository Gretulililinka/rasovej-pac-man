extends Node2D

# obrázek na pozadí a jeho velikost
var obrazek=null
var velikostObrazku=null

#rozlišení vobrazovky
var rozliseni=null

# velkej font titulku a menčí pro vobyč text
var font_titulku=null
var font_textu=null

# nápis
var podtitulek=null

# gui prvek na vyplnění nějakýho textu
var vyplnovadlo_textu=null

# funkce kterou vyrobíme textovej nápis/label
# nastavíme mu font barvu velikost align a strčíme ho na scénu a vrátíme jeho instanci
func vyrob_a_pridej_napis(font,align=Label.ALIGN_CENTER):
	var napis=Label.new()
	napis.set_size(Vector2(rozliseni.x,64))
	napis.set('custom_fonts/font', font)
	napis.add_color_override('font_color', Color(1,0,0))
	napis.align=align
	self.add_child(napis)
	return napis


# umístí na scénu gui prvky pro zapsání novýho nejvyšího skóre
# vyplňování ména se skládá jenom z nápisu a vyplňovadla textu
func novy_zaznam():
	podtitulek=vyrob_a_pridej_napis(font_textu)
	podtitulek.text='Máš novej rekord!!!!\nVyplň méno a máčkni enter!!!!'
	podtitulek.set_position(Vector2(0, rozliseni.y*0.4))
	podtitulek.align=Label.ALIGN_CENTER
	
	vyplnovadlo_textu=LineEdit.new()
	vyplnovadlo_textu.set_size(Vector2(rozliseni.x-128,64))
	vyplnovadlo_textu.set('custom_fonts/font', font_titulku)
	vyplnovadlo_textu.add_color_override('font_color', Color(1,0,0,1))
	vyplnovadlo_textu.set_position(Vector2(64, rozliseni.y/2))
	
	#nastavíme maximální počet znaků ména hráče na 20
	vyplnovadlo_textu.max_length=20
	self.add_child(vyplnovadlo_textu)
	
	#propjíme signál zadání textu (jeto máčknutí enetru při vyplňování) s metodou _on_LineEdit_text_entered
	vyplnovadlo_textu.connect('text_entered',self,'_on_LineEdit_text_entered')
	
	# dáme vyplňovátku 'zaměření'
	vyplnovadlo_textu.grab_focus()

const v_posun=512 # posun všech nápisů v tabulce dolu

# umístí na scénu dvacet nápisů v párech vedle sebe vždycky název hráče a
# kolik jako udělal bodů
func tabulka_nejvysiho_skore():
	
	var text_rekordu=''
	for index in range(10):
		var zaznam=Skore.tabulka_nejvysiho_skore[index]
		var text=str(index+1)+'. '+str(zaznam[0])
		var napis=vyrob_a_pridej_napis(font_textu,Label.ALIGN_LEFT)
		napis.set_position(Vector2(128, index*64+v_posun))
		napis.text=text
		
		napis=vyrob_a_pridej_napis(font_textu,Label.ALIGN_RIGHT)
		napis.set_position(Vector2(-128,index*64+v_posun))
		napis.text=str(zaznam[1])
		
	# nakonec přidáme tlačítko pro návrat na hlavní stranu
	var tlacitko_zpet=Button.new()
	tlacitko_zpet.set('custom_fonts/font', font_textu)
	tlacitko_zpet.add_color_override('font_color', Color(1,0,0,1))
	tlacitko_zpet.text='Zpět'
	tlacitko_zpet.rect_size=Vector2(rozliseni.x/2,64)
	tlacitko_zpet.rect_position.x=rozliseni.x/4
	tlacitko_zpet.rect_position.y=64*11 + v_posun
	
	tlacitko_zpet.connect('pressed',self,'_on_TlacitkoZpet_zmacknuto')
	

	self.add_child(tlacitko_zpet)
	
	tlacitko_zpet.grab_focus()

func _ready():
	
	# načtem a zapnem muziku
	var hudba = AudioStreamPlayer.new()
	hudba.stream=preload('res://zvuky/hudbaMenu.ogg')
	self.add_child(hudba)
	hudba.play()
	
	#načtem *.ttf fonty
	font_titulku = DynamicFont.new()
	font_titulku.font_data = load('res://fonty/font.ttf')
	font_titulku.size = 96
	font_titulku.outline_size=4
	font_titulku.outline_color=Color(0,0,0,1)
	
	font_textu = DynamicFont.new()
	font_textu.font_data = load('res://fonty/font.ttf')
	font_textu.size = 48
	font_textu.outline_size=3
	font_textu.outline_color=Color(0,0,0,1)
	
	
	obrazek=Sprite.new()
	obrazek.texture=preload('res://obrazky/game_over.png')
	rozliseni=get_viewport_rect().size
	velikostObrazku=obrazek.texture.get_size()
	obrazek.centered=false
	obrazek.set_scale(Vector2(rozliseni.x/velikostObrazku.x, rozliseni.y/velikostObrazku.y))
	self.add_child(obrazek)
	
	var titulek=vyrob_a_pridej_napis(font_titulku)
	titulek.text='Nejvyší skóre'
	
	Skore.nacti_nejvysi_skore()
	# nejdřiv kouknem jestli se bude vyplňovat nový skóre do tabulky a jestli jako jo
	# tak umístíme na scénu prvky pro vyplnění ména hráče
	if Skore.ma_se_skore_pridat():
		novy_zaznam()
	else:
		# jinak ukážem tabulku s nejvyšíma skórema
		tabulka_nejvysiho_skore()

# potom co bylo potvrzený méno hráče ve vyplňovadlu textu tak přidáme to
# méno do tabulky uložíme do souboru vodstraníme gui prvek vyplňovadla a podtitulek 
# a ukážem tabulku s highscore
func _on_LineEdit_text_entered(text):
	Skore.pridej_skore_do_tabulky(text)
	Skore.uloz_nejvysi_skore()
	podtitulek.queue_free()
	vyplnovadlo_textu.queue_free()
	
	tabulka_nejvysiho_skore()

#návrat do hlavního menu
func _on_TlacitkoZpet_zmacknuto():
	get_tree().change_scene('res://sceny/HlavniMenu.tscn')
