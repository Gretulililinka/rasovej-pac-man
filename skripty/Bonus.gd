extends 'res://skripty/VecNaMape.gd'

class_name Bonus

# existujou čtyry druhy bonusu
# iphone, krabice nike televize apak molotvej koktejl
enum {IPHONE,NIKE,TV,MOLOTOV}

# signály hlavní scéně jestli jako přidat nějaký body nebo aktivovat vyděšenej režim duchů
signal pridej_body(pocet)
signal molotov

var hrac=null
var atlas=null
var animace=null
const sirka_atlasu=8
	
var druh=0
var pocet_bodu=0

# jóó teroreticky bysme mohli dědit metodu ztý společný třídy 'VecNaMape' když ji máme i 
# ve 'Figurce'. mi tam ale atlas nějak neštymuje ktý třídě 'VecNaMape' by se asi
# jako měla menovat nějak jinak....... :O :O :/ :/
# než se ale jako voběktový vorientálci navzájem ňák jakoby dohodnou já už tam
# budu mit metodu nahrubo copypastnutou :O :O fuj!!!! :O :O :D :D ;D
func textura_z_atlasu_indexem(index):
	var textura=AtlasTexture.new()
	textura.atlas=atlas
	textura.region = Rect2((index % sirka_atlasu) * 64, (index / sirka_atlasu) * 64, 64, 64)
	return textura

# každá ta věc jako má jednu animaci kdy se jakoby točí namístě :O ;D
func vygeneruj_z_atlasu_animaci(prvni_snimek,posledni_snimek,nazev,fps=6):
	var seznamAnimaci=self.animace.get_sprite_frames()
	if seznamAnimaci==null:
		seznamAnimaci=SpriteFrames.new()
		
	seznamAnimaci.add_animation(nazev)
	
	for i in range(prvni_snimek,posledni_snimek):
		seznamAnimaci.add_frame(nazev,textura_z_atlasu_indexem(i))
		
	seznamAnimaci.set_animation_loop(nazev,true)
	seznamAnimaci.set_animation_speed(nazev,fps)
	self.animace.set_sprite_frames(seznamAnimaci)

# pokud je hráč na stený souřadnic  a bonus je viditelnej takže sebratelnej
# nóó tak ho jako seberem
func _process(delta):
	if self.visible and hrac.logicka_pozice==logicka_pozice:
		sebrano()

func napojit_signaly(koren):
	self.connect('molotov',koren,'_on_Bonus_molotov')
	self.connect('pridej_body',koren,'_on_Bonus_pridej_body')

# konstruktor
func _init(druh_bonusu,pozice,hrac):
	
	druh=druh_bonusu
	logicka_pozice=pozice
	self.hrac=hrac
	self.position.x=logicka_pozice.x*64+32
	self.position.y=logicka_pozice.y*64+32
	
func nastav_druh(druh):
	self.druh=druh
	match druh:
		IPHONE:
			pocet_bodu=100
			self.animace.play('iphone')
		NIKE:
			pocet_bodu=200
			self.animace.play('nike')
		TV:
			pocet_bodu=300
			self.animace.play('tv')
		MOLOTOV:
			self.animace.play('molotov')
			
func _ready():
	
	atlas=preload('res://obrazky/ruzne.png')
	self.animace=AnimatedSprite.new()
	self.add_child(self.animace)
	
	#jeto vždycky po devíti index těch vobrázků
	vygeneruj_z_atlasu_animaci(2,11,'iphone')
	vygeneruj_z_atlasu_animaci(20,29,'nike')
	vygeneruj_z_atlasu_animaci(29,38,'tv')
	vygeneruj_z_atlasu_animaci(11,20,'molotov')
			
	self.nastav_druh(self.druh)

# volaný když hráč bonus sebere
func sebrano():
	# dáme vědět hlavní scénce
	if druh != MOLOTOV:
		emit_signal('pridej_body',pocet_bodu)
	else:
		emit_signal('molotov')
	#zneviditelníme oběkt jakože je sebranej
	self.visible=false
