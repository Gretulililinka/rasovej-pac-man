extends 'res://skripty/Duch.gd'

func _ready():
	rozptylova_pozice=Vector2(0,28)
	vygeneruj_animace(54,'',self.snimku_za_vterinu)
	barva=Color('#cec545')

#oranžovej duch normálně pronásleduje pacmana ale jakmile k sobě přídou blíž než na 8
#políček si vybere jako cíl svuj rozptylovej bod
func urci_cil_pronasledovani():
	if self.logicka_pozice.distance_to(hrac.logicka_pozice)<8:
		cilova_pozice=rozptylova_pozice
	else:
		cilova_pozice=hrac.logicka_pozice
