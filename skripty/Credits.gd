extends Control

var rozliseni

func vyrob_a_pridej_napis(font,align=Label.ALIGN_CENTER):
	var napis=Label.new()
	napis.set_size(Vector2(rozliseni.x,64))
	napis.set('custom_fonts/font', font)
	napis.add_color_override('font_color', Color(1,0,0))
	napis.align=align
	self.add_child(napis)
	return napis

func _ready():
	# načtem a zapnem muziku
	var hudba = AudioStreamPlayer.new()
	hudba.stream=preload('res://zvuky/hudbaMenu.ogg')
	self.add_child(hudba)
	hudba.play()
	
	var obrazek=Sprite.new()
	obrazek.texture=preload('res://obrazky/main_menu.png')
	var sirka = 1344
	var vyska = 1792
	var velikostObrazku=obrazek.texture.get_size()
	obrazek.centered=false
	obrazek.set_scale(Vector2(sirka/velikostObrazku.x, vyska/velikostObrazku.y))
	self.add_child(obrazek)
	
	#načtem *.ttf fonty
	var font_titulku = DynamicFont.new()
	font_titulku.font_data = load('res://fonty/font.ttf')
	font_titulku.size = 96
	font_titulku.outline_size=4
	font_titulku.outline_color=Color(0,0,0,1)
	
	var font_textu = DynamicFont.new()
	font_textu.font_data = load('res://fonty/font.ttf')
	font_textu.size = 30
	font_textu.outline_size=3
	font_textu.outline_color=Color(0,0,0,1)
	
	rozliseni=get_viewport_rect().size
	
	var titulek=vyrob_a_pridej_napis(font_titulku)
	titulek.text='Credits'
	
	var textik=vyrob_a_pridej_napis(font_textu)
	textik.set_position(Vector2(0,64*9))
	textik.text='V Godotu udělala Gréta volně podle \n'
	textik.text+='originálního pacmana vod číňana Tora Iwataniho,\n\n'
	textik.text+='zvuky sou vod Little Robot Sound Factory,\n www.littlerobotsoundfactory.com\n\n'
	textik.text+='hudba je vod TinyWorlds, Tyrfinga a Daniela Stephense,\n všecko z opengameart.org\n\n'
	textik.text+='font je vocaď https://fontmeme.com/fonts/akadylan-font\n\n'
	textik.text+='joa vobrázky sem dělala/upravovala sama,\nvšecko blender/krita/koulourpaint :D ;D'
	textik.text+='\n\n\npodobnost s jakoukoliv živou vosobou nebo zvířetem\n je jakože fakt čistě náhodná :O ;D\n\n'
	textik.text+='hra byla udělaná za účelem podpoření americkejch\nrasovejch nepokojů a pouliční civilní války\n'
	textik.text+='#BLEK.livesMatter!!!!!!!!!!!! :O :D :D ;D'
	
	var tlacitko_zpet=Button.new()
	tlacitko_zpet.set('custom_fonts/font', font_textu)
	tlacitko_zpet.add_color_override('font_color', Color(1,0,0,1))
	tlacitko_zpet.text='Zpět'
	tlacitko_zpet.rect_size=Vector2(rozliseni.x/2,64)
	tlacitko_zpet.rect_position.x=rozliseni.x/4
	tlacitko_zpet.rect_position.y=64*25
	
	tlacitko_zpet.connect('pressed',self,'_on_TlacitkoZpet_zmacknuto')
	

	self.add_child(tlacitko_zpet)
	
	tlacitko_zpet.grab_focus()
	
#návrat do hlavního menu
func _on_TlacitkoZpet_zmacknuto():
	get_tree().change_scene('res://sceny/HlavniMenu.tscn')
