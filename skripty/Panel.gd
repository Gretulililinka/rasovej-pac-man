extends Control


# jako počet životů budem používat texturu s obrázkem pacmanovy čokoládový tvářičky 
var textura_hlavy=null
# do pole 'hlavy' budem nastrkávat sprity s vobrázkem pacmanovy hlavy každá bude 
# reprezentovat jeden život
var hlavy=[]

# pozadí panelu
# je to jakoby jenom takovej černej obdélníček
var pozadi=null

# nápisy který řikaj počet bodů a jakej je právě level
var skore_napis=null
var level_napis=null

func _ready():
	pozadi=ColorRect.new()
	pozadi.rect_size.x=21*64
	pozadi.rect_size.y=64
	pozadi.color=Color(0,0,0,1)
	self.add_child(pozadi)
	
	#načtem *.ttf font
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load('res://fonty/font.ttf')
	dynamic_font.size = 32
	
	# a vyrobíme a přidáme ukazatele skóre a jakej je level
	skore_napis=Label.new()
	skore_napis.set_size(Vector2(640,64))
	skore_napis.set('custom_fonts/font', dynamic_font)
	skore_napis.add_color_override('font_color', Color(1,0,0,1))
	pozadi.add_child(skore_napis)
	
	level_napis=Label.new()
	level_napis.set_size(Vector2(640,64))
	level_napis.set('custom_fonts/font', dynamic_font)
	level_napis.add_color_override('font_color', Color(1,0,0,1))
	level_napis.set_position(Vector2(640,0))
	pozadi.add_child(level_napis)
	
	# z atlasu si načteme podobrázek s obličejčkem pacmana
	var atlas=preload('res://obrazky/hrac.png')
	textura_hlavy=AtlasTexture.new()
	textura_hlavy.atlas=atlas
	textura_hlavy.region=Rect2(Vector2(0,0),Vector2(64,64))

	# mužem nastavit nějaký výchozí hodnoty
	nastav_skore(0)
	nastav_level(1)
	 
func nastav_level(hodnota):
	level_napis.text='Level: '+str(hodnota)
	
func nastav_skore(hodnota):
	skore_napis.text='Skóre: '+str(hodnota)
	
# přidání života je vygenerování novýho spritu s vobrázkem hlavy a umístění ji na panel
# vodebrání ji zase bude ničit
func pridej_zivot():
	var hlava=Sprite.new()
	hlava.texture=textura_hlavy
	hlava.position.y=32
	hlava.position.x=(21-hlavy.size())*64-32
	hlavy.append(hlava)
	add_child(hlava)
	
func odeber_zivot():
	#odstraníme poslední prvek z pole i ze stromu uzlů
	remove_child(hlavy.pop_back())
