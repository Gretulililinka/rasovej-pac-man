extends 'res://skripty/Duch.gd'

var cervenejDuch=null

#samo že mě tady napadlo udělat poděděnej konstruktor nastav() kterej by jako navíc bral 
# argument červenej duch nóó ale nesnese to když se poděděná metoda menuje stejně ale má
# jinej počet argumentů :O :O :'( :'(

func _ready():
	rozptylova_pozice=Vector2(20,28)
	vygeneruj_animace(0,'',self.snimku_za_vterinu)
	barva=Color(0.0,1.0,1.0,1.0)
	#jdi(LEVA)

# azurovej duch je lump nejvěčí!!!! :O :O
# narozdíl vod vostatních duchů umí jednoduchej vodhad pacmanovi budoucí pozice
# vezme se pozice pronásledovacího červenýho ducha a pozice dvě políčka před pacmanem
# mezi těmadle dvouma bodama se udělá vektor jeho dýlka se zdvojnásobí
# pak se přičte pozice tamtoho červenýho pronásledovacího ducha a máme cílovou pozici
# azurovýho ducha

func urci_cil_pronasledovani():
	
	# dvě políčka před hráčem
	var pozice_hrace=hrac.logicka_pozice + hrac.predchozi_smer*2
	
	# vektorovej výpočet (pacman-cervenej_duch)*2+cervenej_duch zjednodušíme na 2*pacman-cervenej_duch
	cilova_pozice=pozice_hrace*2-cervenejDuch.logicka_pozice
