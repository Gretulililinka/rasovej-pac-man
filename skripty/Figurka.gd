extends 'res://skripty/VecNaMape.gd'

class_name Figurka

#sou to floaty ale mužeme snima hezky prasečit jakože porovnávat vectory == nebo
# různý směry kerejma se muže figurka vydat
const NAHORU = Vector2(0.0,-1.0)
const DOLU = Vector2(0.0,1.0)
const PRAVA = Vector2(1.0,0.0)
const LEVA = Vector2(-1.0,0.0)
const STOJI_NA_MISTE = Vector2(0.0,0.0)

# dlaždicová mapa s vokrajema bludiště
var okraj=null

# jak dlouho figurka dělá v tweenu krok
var doba_kroku=0

var atlas=null

# jestli figurka právě někam de. když je tahleta proměná 'true' tak bysme jako neměli
# dělat nějakej další krok
var jde=false

# jakej byl směr v posledním kroku
var predchozi_smer=DOLU

# název animací jednotlivejch akcí kroků
var animace_nahoru='nahoru'
var animace_dolu='dolu'
var animace_prava='prava'

# výška obrázku v atlasu
# má smysl při generování animací
var vyska_obrazku=128

# všechny animace oběktu
var animace=null

# tween na dělání kroků figurky
var tweenKroku=null

# i přes usilovný hledání se mi nepodařilo najít funcki která by jako vracela
# region z atlasu podle indexu nóó tak sem to udělala takle.
# předpokládám že jakoby exituje nějakej ofiko způsob kterej je víc lepší ale 
# nenašla sem prostě :O :O :'( :'(
var sirka_atlasu=16
func textura_z_atlasu_indexem(atlas,index):
	var textura=AtlasTexture.new()
	textura.atlas=atlas
	textura.region = Rect2((index % sirka_atlasu) * 64, (index / sirka_atlasu) * vyska_obrazku, 64, vyska_obrazku)
	return textura

# funkce kterou uděláme figurkou krok zadaným směrem
# směr je jeden z těch čtyr vektorů nazačátku tohodletoho souboru
func jdi(kam):
	
	# začnem přehrávat vodpovídající animaci k chůzi daným směrem
	if kam==DOLU:
		animace.play(animace_dolu)
	elif kam==NAHORU:
		animace.play(animace_nahoru)
	elif kam==PRAVA:
		animace.play(animace_prava)
	elif kam==LEVA:
		#todleto neni chyba!!!!! :O :O
		#když de figurka doleva použije se animace chůze doprava a nastavíme
		#flip_h na true abyse obrázek horizontálně překlopil :O ;D 
		animace.play(animace_prava)
		animace.flip_h=true
		
	if kam!=LEVA:
		# noa když se nejde doleva tak dáme flip_h na vypnutej
		animace.flip_h=false
	
	# nakonec uděláme přesun tý figurky pomocí tweenu
	# do tweenu budem strkat realnou pozici né logickou
	# cílovej bod získáme vynásobením vektoru dýlkou hrany jedny flaždičky + současná realná pozice
	var cil = self.position + (kam*64)
	
	tweenKroku.interpolate_property(self, 'position', position, cil, doba_kroku, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tweenKroku.start()
	
	jde = true
	predchozi_smer = kam

# funkce co ověřuje jestli se figurka muže vydat nějakým zadaným směrem
func muzu_jit(kam):
	
	#vektory mužeme takle sčítat dohromady plusem :O ;D
	var pozice = logicka_pozice + kam
	
	#koukneme se jestli v mapě na daný souřadnici existuje dlažička
	# a jestli jako jo tak tam jít nemužem
	return okraj.get_cellv(pozice)==TileMap.INVALID_CELL
	
func _ready():
	animace=AnimatedSprite.new()
	self.add_child(animace)
	
	tweenKroku=Tween.new()
	self.add_child(tweenKroku)
	tweenKroku.connect('tween_completed',self,'_on_TweenKroku_tween_completed')

	animace.flip_h=false
	

#metoda která se zavolá až tween kroku zkončí
#názvy funkcí který sou napojený na signály by měli mit takovejdle formát
# _on_<název uzlu>_<název signálu>
#musí brát tyhlencty dva argumenty
# první je objekt na kterým se tween dělá a druhej je atribut kterej se mění
func _on_TweenKroku_tween_completed(objekt,key):
	
	animace.stop()
	
	# teleportace když věc vyleze dírou mimo vobrazovku
	# mi tu figurku jakoby teleportujem na druhou stranu a uděláme další krok aby
	# jakože ztý druhý strany vylezla
	if logicka_pozice.x <= -1:
		logicka_pozice.x = 21
		self.position.x = (logicka_pozice.x+1) * 64 - 32
		jdi(predchozi_smer)
		
	elif logicka_pozice.x >= 21:
		logicka_pozice.x = -1
		self.position.x = (logicka_pozice.x+1) * 64 - 32
		jdi(predchozi_smer)
	
	else:
		#pokud neni figurka v teleportu muže dělat krok podle sebe jak jako chce
		jde = false

#funkce stuj slouží k zastavení figurky abysme mohli třeba ručně změnit jeji polohu a tak
# předevšim nahrubo zastavíme tween kroku a proměnou jde na false
# takle zabráníme volání callbacku _on_TweenKroku_tween_completed a nějakýho dalšího co dělá s proměnou 'jde'
# tyhlety věci nám třeba mužou s figurkou hejbat potom co byse nastavila jeji nová poloha a
# nedělalo byto jako dobrotu :O :O
func stuj():
	tweenKroku.stop(self)
	jde=false

# nastavuje názvy animací pro směry nahoru doprava a dolu
# základní se tak menujou ale duch tam má třeba navíc ještě jednu sadu animací pro sežratelnej/vyděšenej stav
func prepni_animaci(prefix=''):
	animace_nahoru=prefix+'nahoru'
	animace_dolu=prefix+'dolu'
	animace_prava=prefix+'prava'
