extends 'res://skripty/Figurka.gd'
# duch dědí z figurky

class_name Duch

# duch má tři režimy pohybu
# 'pronásledování' kdy se pohybuje nějak podle souřadice pacmana
# v tom se ty různý duši lišej a každej to dělá nějak jinak
# režim 'rozptýlení' kdy se pohybuje na nějakou předem danou nedosažitelnou pozici
# 'pronásledování' a 'rozptýlení' se střídaj v různejch časovejch intervalech
# poslední je 'vyděšenej' když pacman sežere bonus. si duch pak vybírá směr náhodně na každý křižovatce
# když vyleze z domečku de vždycky doleva
# duch skoro vubec necouvá jediná vyjímka je když se přepne režim duchů z 'pronásledování' nebo 'rozptýlení' do jinýho
#	pak couvat musí :O :O táákže jedinej režim ze kterýho když se duch jakoby přepne a nezačne couvat je 'vystrašení'
# na mapě sou čtyry křižovatky ve kterejch duch nesmí při běžným rozhodování zahnout nahoru

# kromě těch tří režimů z pravidel ještě jakoby musíme vošetřit situace kdy je duch sežranej a de jako domu
# kdy je doma a připravenej na vypuštění nóó akdy z domečku vychází
enum RezimPohybu {ROZPTYLENI, PRONASLEDOVANI, VYDESENI, DOMU, JE_DOMA, VYPOUSTENI}

# duchův nastavenej aktuální režim pohybu 
var rezim=RezimPohybu.ROZPTYLENI

# požadovanej režim pohybu
# do něj se duch přepne při nejbliší možný příležitosti.toje pro situace kdy třeba
# duch právě přestal bejt vystrašenej a je potřeba pokračovat tam kde jakoby přestal
var pozadovany_rezim_pohybu=RezimPohybu.JE_DOMA

# pozice na kterou se duch pořád jakoby snaží dostat
var cilova_pozice

# rozptylová pozice na kterou si duch vybere jako cílovou v režimu rozptýlení
# každej duch má svou jinou rozptylovou pozici
var rozptylova_pozice

# logický souřadnice různejch pro ducha zajímavejch míst v bludišti 
const DOMECEK_POZICE=Vector2(10,13)
const VYPOUSTECI_POZICE=Vector2(9,10)
const POZICE_PRED_DOMECKEM=Vector2(10,10)

# jestli se jako votočí po příštím kroku
var otoc_se=false

# reference na voběkt hráče aby duch moh vědět jestli se s nim zdrcnul
var hrac=null

# rychlost animace a počet jeji snímků
const snimku_za_vterinu=20.0
const kolik_ma_animace_snimku=10.0

# jak moc jakože duch zpomalí když má strach žeby i u něj doma mohla přistát zápalná flaška
const zpomaleni=1.2
var exploze=null

# jakouže má duch barvu. má význam při vytváření instance exploze abyto jako k sobě štimovalo
var barva=null

# jestli duch bliká jestli je právě bliknutej kolik uteklo doby od bliknutí a jak jako bliknutí má dlouho trvat
var blika=false
var bliknuty=false
var cas_blikani=0.0
const DOBA_BLIKNUTI=0.1

# signály co duch posílá do herní scény 'Hra.tscn'

# signál když ho hráč sežere
signal sezranej
# signál když duch dorazí domu. signál v sobe má skovanou informaci kterejže duch to jako je
signal doma(ktery)
# signál kdyý zakleknul hráče
signal zakleknuti

# křižovatky na kterejch duch normálně nemůže zabočit nahoru
var specialni_krizovatky = [Vector2(9.0,10.0),Vector2(11.0,10.0),Vector2(9.0,20.0),Vector2(11.0,20.0)]

# věc  co dělá že zní padaj skoro uplně náhodný čísla :O ;D
var generatorNahodnychCisel = RandomNumberGenerator.new()

# efekt výbuchu
const VYBUCH_SKRIPT = preload('res://skripty/Exploze.gd')

# napojíme posílaný signály na vodpovídající callbacky v herní scénce
# ta se tam strká jako kořen ke kterýmu seto bude napojovat
func napojit_signaly(koren):
	self.connect('sezranej',koren,'_on_Duch_sezranej')
	self.connect('doma',koren,'_on_Duch_doma')
	self.connect('zakleknuti',koren,'_on_Duch_zakleknuti')

# nastavování atributů co by normálně dělal konstruktor je řešený toudletou metodou
# oběkt budem několikrát přenastavovat takže mito připadá lacinější než vyrábět furt nový a nový
func nastav(okraj,hrac,logicka_pozice,rezim_pohybu):
	self.okraj=okraj
	self.hrac=hrac
	self.logicka_pozice=logicka_pozice
	self.position=(logicka_pozice*64) + Vector2(32,32)
	self.rezim=rezim_pohybu
	self.pozadovany_rezim_pohybu=RezimPohybu.JE_DOMA
	self.cilova_pozice = VYPOUSTECI_POZICE
	self.predchozi_smer=Figurka.DOLU

# funkce co nám z atlasu vygeneruje potřebný animace ducha
#  když se na vobrázek s tim atlasem duchů mrknete tak uvidíte že ty duchové sou jednak
# srovnaný za sebou podle barev (dovopravdy teda jako podle názvů ale pšššt:D ;D) nóó a taky taky že 
# sou seřazený ty jednotlivý animace u každýho ducha pokaždý stejně

# no budem je brát z toho atlasu podle počátečního indexu 
func vygeneruj_animace(pocatecni_index,prefix,fps):
	
	# vezmem z animace seznam animací
	var seznamAnimaci=animace.get_sprite_frames()
	
	# pokud jeto nulová instance nóó tak vyrobíme novou
	if seznamAnimaci==null:
		seznamAnimaci=SpriteFrames.new()
		
	# přidáme názvy animací do seznamu 
	seznamAnimaci.add_animation(prefix+'dolu')
	seznamAnimaci.add_animation(prefix+'nahoru')
	seznamAnimaci.add_animation(prefix+'prava')
	
	# noa teďko budem přidávat samotný snímky animací
	var index=pocatecni_index
	# přidáme animaci v seznamu s vodpovídajícím názvem texturu podle indexu
	for i in range(6):
		seznamAnimaci.add_frame(prefix+'dolu',textura_z_atlasu_indexem(atlas,i+index))
		
	# pak ještě přidáme pozpátku snímky mimo prvního a posledního aby se nám animace jakože 
	# vracela zpátky nazačátek
	for i in range(1,5):
		seznamAnimaci.add_frame(prefix+'dolu',textura_z_atlasu_indexem(atlas,(5-i)+index))
		
	# posunem se v tom vobrázku zvětšením indexu a berem další animaci.....
	index+=6
	for i in range(6):
		seznamAnimaci.add_frame(prefix+'prava',textura_z_atlasu_indexem(atlas,i+index))
	for i in range(1,5):
		seznamAnimaci.add_frame(prefix+'prava',textura_z_atlasu_indexem(atlas,(5-i)+index))
		
	index+=6
	for i in range(6):
		seznamAnimaci.add_frame(prefix+'nahoru',textura_z_atlasu_indexem(atlas,i+index))
	for i in range(1,5):
		seznamAnimaci.add_frame(prefix+'nahoru',textura_z_atlasu_indexem(atlas,(5-i)+index))
	
	# nakonec nastavíme rychlosti přehrávání těch animací
	seznamAnimaci.set_animation_speed(prefix+'dolu',fps)
	seznamAnimaci.set_animation_speed(prefix+'prava',fps)
	seznamAnimaci.set_animation_speed(prefix+'nahoru',fps)
	
	# a nastavíme je zpátky tomu voběktu
	animace.set_sprite_frames(seznamAnimaci)

func _ready():
	#bod [0,0] spritu leží v jeho středu
	# my chceme aby duch jakoby stál na jednom políčku a vyčuhoval jakoby nahoru
	#posuneme počátek o 32 teda půlku velikosti čtverečku nahoru
	animace.offset.y = -32
	# načtem atlas všech vobrázků s duchama
	atlas = preload('res://obrazky/duchove.png')
	
	# atlas je širkoej šestnáct vobrázků našířku a vobrázek je vysokej 128 pixelů
	# tyhlety hodnoty sou důležitý aby nám jako fungovala metoda 'textura_z_atlasu_indexem()' 
	# poděděná z rodičovský třídy 'Figurka'
	sirka_atlasu = 16
	vyska_obrazku = 128
	
	# krok přesunu figurky ducha nám trvá upně stejně dlouho jako doba přehraní animace kroku
	doba_kroku = kolik_ma_animace_snimku / snimku_za_vterinu
	
	# před používáním generátoru náhodnej šísel ho musíme jakože iniciialioizovat
	#jako seed je strčenej čas právě teď
	generatorNahodnychCisel.randomize()

	vygeneruj_animace(75,'sezratelnej_',snimku_za_vterinu)
	
	var seznamAnimaci=animace.get_sprite_frames()
	
	seznamAnimaci.add_animation('voci_dolu')
	seznamAnimaci.add_frame('voci_dolu',textura_z_atlasu_indexem(atlas,72))
	seznamAnimaci.set_animation_speed('voci_dolu',1)
	
	seznamAnimaci.add_animation('voci_nahoru')
	seznamAnimaci.add_frame('voci_nahoru',textura_z_atlasu_indexem(atlas,73))
	seznamAnimaci.set_animation_speed('voci_nahoru',1)
	
	seznamAnimaci.add_animation('voci_prava')
	seznamAnimaci.add_frame('voci_prava',textura_z_atlasu_indexem(atlas,74))
	seznamAnimaci.set_animation_speed('voci_prava',1)
	
	animace.play('sezratelnej_dolu')
	
	barva=Color(0.0,0.0,1.0,1.0)
	exploze=load('res://sceny/veci/Exploze.tscn')


func _process(delta):
	
	# když končí efekt molotovu tak duch zčne blikat jakože zase přepne sou barvu
	# z modrý na původní a bude zase pro pacmana děsně nebezpečnej
	if blika:

		#budem si skovávat do proměný cas_blikani uplinutou dobu nóó a až když 
		# jakoby překroční nějakou tu mez tak přepnem zvhled ducha 
		# kterej zhled je aktivní řeší jako proměná 'bliknuty'
		if cas_blikani<DOBA_BLIKNUTI:
			cas_blikani+=delta
		else:
			if bliknuty:
				blikni_na_normalni()
			else:
				blikni_na_sezratelnej()
			cas_blikani=0.0
			bliknuty = !bliknuty
		
	
	# budem čekat na kolizi ducha s hráčem a když seto stane tak buďto duch hráče
	# zaklekne nóó a nebo když je duch vystrašenej tak hráč zpapá :O ;D
	
	#pokud bude zdálenost hráče a ducha menčí než dvacetina jejich velikosti budem to
	# jakoby považovat za kolizi těch dvou figurek
	# možná vás napadá žeby jako stačilo porovnávat jestli jako sou jejich logický pozice stejný
	# jakože jestli self.logicka_pozice==hrac.logicka_pozice. jenže vona poměrně často nastávala situace
	# že duch a hráč ze sousedních políček udělaj krok proti sobě ve stejnej vokamžik a normálně
	# jako skrz sebe projdou :O :O dělá tota skoková změna těch logickejch souřadnic
	# buťto bysme museli přidat nějakou logiku navíc coby jako hlídala žese to stane nóó
	# a nebo to udělat takle 'fyzykálně' :O ;D
	if self.position.distance_to(hrac.position)<12.8:
		
		# pokud je duch vyděšenej tak ho pacman muže sníst
		if rezim == RezimPohybu.VYDESENI:
			
			# pošlem signál do herní scény 'Hra.tscn' žeje duch sežranej pacmanem
			emit_signal('sezranej')
			
			# uděláme modrej výbuch a dáme ho do stromu
			var bum = VYBUCH_SKRIPT.new(self.position,Color(0.0,0,1.0,1.0))
			get_parent().add_child(bum)
			animace.stop()
			# přepnem vobrázek ducha na voči
			# voči nejsou žádná pořádná animace jeto jenom jeden jedinej vobrázek
			# pro každej ten směr 
			self.prepni_animaci('voci_')
			animace.play('voci_dolu')
			modulate.a = 1.0 # vypnutí modulace alpha kanálu aby jakože nebyl průsvitnej
			
			# zrychlíme a pošlem domu
			doba_kroku = 0.25
			cilova_pozice = DOMECEK_POZICE
			rezim = RezimPohybu.DOMU
		
		# noa jestli je duch v rozptylovacím nebo pronásledovacím režimu tak
		# pacmana normálně zabije :O :O :O :O
		# von ho teda nezabije ten duch duch jenom zase pošle signál do herní scény
		# a ta si to už nějak jakoby udělá :O :D ;D ;D
		elif rezim < RezimPohybu.DOMU:
			if not hrac.zakleknuty:
				emit_signal('zakleknuti')

# zapnutí režimu rozptýlení
# v něm de duch na svou rozptylovou pozici
func nastav_rezim_rozptyleni():
	
	# pokuť je duch doma de domu nebo právě vylejzá režim se zapne při příští příležitosti
	if rezim>RezimPohybu.VYDESENI:
		pozadovany_rezim_pohybu=RezimPohybu.ROZPTYLENI
		return

	# při první příležitosti se duch MUSÍ votočit a jít zpátky
	otoc_se=true
	
	rezim=RezimPohybu.ROZPTYLENI
	cilova_pozice=rozptylova_pozice
	
#ducha jako nejde vyděsit když se skovává v domečku 
# nebo když je sežranej a mrtvej se vrací domu
# pro vycházení jeto vymyšlený tak žeho de vyděsit jenom kdyžuž je za dveřma
# teda když má stejnou ypsilonovou souřadnici jako má vypoštěcí pozice
func nastav_rezim_vydeseni():
	if rezim == RezimPohybu.VYPOUSTENI and logicka_pozice.y != VYPOUSTECI_POZICE.y:
		return
	elif rezim>RezimPohybu.VYDESENI:
		return
	
	if rezim != RezimPohybu.VYDESENI:
		
		# až vyděšení skončí se zase vrátí do předchozího režimu v jakým byl
		pozadovany_rezim_pohybu = rezim
		rezim = RezimPohybu.VYDESENI
		
		# zprůhledníme zpomalíme a votočíme
		modulate.a = 0.75
		doba_kroku *= zpomaleni
		otoc_se = true
		
	else:
		# pokud už vyděšenej jakoby je tak akorát vypnem případný blikání
		blika = false
		
	#nastavíme zhlez spritu ducha na sežratelnej jakože modrý průsvitný animace
	blikni_na_sezratelnej()

# ukončení režimu vyděšení
# duch se vrátí do režimu ve kterým byl a přepne svuj zvhled na normální
func ukonci_rezim_vydeseni():
	if rezim != RezimPohybu.VYDESENI:
		return
	self.normalni_zhled()
	rezim=pozadovany_rezim_pohybu
	
# uplně stejný jako s režimem rozptýlení
func nastav_rezim_pronasledovani():
	
	if rezim > RezimPohybu.VYDESENI:
		pozadovany_rezim_pohybu = RezimPohybu.PRONASLEDOVANI
		return

	otoc_se = true
	rezim = RezimPohybu.PRONASLEDOVANI
	
#duch z domečku dycky de doleva
#pošlem ho nalevo vod dveří a pak ať si stim nějak poradí sám
func vypustit_ducha():
	cilova_pozice = VYPOUSTECI_POZICE
	rezim = RezimPohybu.VYPOUSTENI
	
# nastavení normálního zvhledu ducha
# vypnem případný blikání průhlednost 
# nastavíme výchozí animace jakože 'nahoru' 'dolu' a 'prava'
# a nastavíme normální rychlost pohybu
func normalni_zhled():
	blika = false
	modulate.a = 1.0
	self.prepni_animaci()
	doba_kroku = kolik_ma_animace_snimku / snimku_za_vterinu

# zapnutí blikání ducha
func rozblikat():
	#rozblikat mužem jenom vystrašenýho ducha
	if rezim == RezimPohybu.VYDESENI:
		blika=true
		bliknuty=false
		cas_blikani=0.0

# přepnem zvhled ducha z toho jakej právě okamžitě je na sežratelnej jakože
# modrej
func blikni_na_sezratelnej():
	#nemůžeme blikat když je duch v domečku
	if rezim!=RezimPohybu.VYDESENI:
		return
	
	# musíme přepnou současnou aktuální animaci bez toho aby se jakoby zastavila
	# skováme si index/pořadí právě hranýho vokýnka animace do proměný 'okynko_animace'
	var okynko_animace=self.animace.get_frame()
	
	# vyberem aktivní animaci sežratelnýho ducha ze seznamu podle toho
	# kam jakoby má namířeno
	match self.predchozi_smer:
		NAHORU:
			self.animace.play('sezratelnej_nahoru')
		PRAVA, LEVA:
			self.animace.play('sezratelnej_prava')
		DOLU:
			self.animace.play('sezratelnej_dolu')
	
	# a v animaci dáme jako aktuální okýnko animace okýnko se samým pořadím/indexem
	# jako tam bylo předtim v tý minulý animaci
	# voni se ty animace překrejvaj takže to bude vypadat že ten duch jenom zněnil
	# svou barvičku 
	self.animace.set_frame(okynko_animace)
	
	# pro další hraní zapnem sadu animací s perexem 'sezratelnej_'
	# to sou ty modrý
	self.prepni_animaci('sezratelnej_')
	
func blikni_na_normalni():
	#nemůžeme blikat když je duch v domečku
	if rezim!=RezimPohybu.VYDESENI:
		return
	
	# vezmeme index vokýnka právě hraný animace
	var okynko_animace=self.animace.get_frame()
	# uplně fuj způsobem ufiknem tamten substring 'sezratelnej_' z názvu animace aco
	# zbyde dáme hrát
	# ty animace se menujou uplně stejně akorátže sežratelný tam maj před názvem ten kus s podtržítkem
	self.animace.play(self.animace.get_animation().substr(len('sezratelnej_')))
	
	# a u přepnutý animace dáme stejný vokýnko
	self.animace.set_frame(okynko_animace)
	
	# nakonec aktivujem výchozí sadu animací pro další hraní
	self.prepni_animaci()

# s timdletim budou dělat něco až dětský třídy
# každej duch si to dělá jakoby posvým a uplně jinak :O :O
func urci_cil_pronasledovani():
	pass
	
# metoda kterou se duch jakože rozhoduje coje pro něj jako nejkratčí cesta k cíli
# nóóóó neni moc chytrej :O :O :D ;D
# duch má jedno až tři sousední políčka na který muže jít (couvat nesmí) noa zná souřadnici svýho cíle
# změří euklidovskou zdálenost všech těch políček a cíle noa vybere si to políčko
# kde mu to jakože vyšlo nejmíň
func urci_nejkraci_smer_k_cili():
	var vzdalenost=INF #tamto INF je jakože nekonečno. prostě max možná hodnota čísla
	var smer=null
	
	#jestli chceme jít s duchem nahoru se nejdřiv musíme kouknout jestli jako nejsle na tý nějaký extra křižovatce
	if predchozi_smer != DOLU and not specialni_krizovatky.has(logicka_pozice) and muzu_jit(NAHORU):
		vzdalenost=cilova_pozice.distance_to(logicka_pozice+NAHORU)
		smer=NAHORU
		
	if predchozi_smer != LEVA and muzu_jit(PRAVA):
		var nova_vzdalenost=cilova_pozice.distance_to(logicka_pozice+PRAVA)
		# pokud nemáne ještě vybranej směr nebo je nová vzdálenost menčí než předchozí tak políčko vyberem
		if smer == null or vzdalenost > nova_vzdalenost:
			vzdalenost = nova_vzdalenost
			smer = PRAVA
			
	if predchozi_smer != NAHORU and muzu_jit(DOLU):
		var nova_vzdalenost = cilova_pozice.distance_to(logicka_pozice+DOLU)
		if smer == null or vzdalenost > nova_vzdalenost:
			vzdalenost = nova_vzdalenost
			smer = DOLU
			
	if predchozi_smer != PRAVA and muzu_jit(LEVA):
		var nova_vzdalenost = cilova_pozice.distance_to(logicka_pozice+LEVA)
		if smer == null or vzdalenost > nova_vzdalenost:
			vzdalenost = nova_vzdalenost
			smer = LEVA
			
	return smer
	

# funkce co zišťuje jestli duch jako muže nějakým zadaným směrem udělat krok dopředu
func muzu_jit(smer):

	#duch nám nesmí projít jentak dveřma domečku
	#muže jenom když ho vypouštíme nebo se jako sežranej vrací
	#procházení ducha dveřma
	#chceme aby se dveře někdy chovali jako překážka noa někdy tam zase jako vubec nebyly
	#duch muže projít jenom když se vracej voči domu nebo ho vypouštíme z domečku
	#dveře sou na pozici (10,11)
	# poznámka - todleto nijak nesouvisí vobrázkem dveří v bludišti a jejich votvíráním. toje
	# 	řízený metodou objektu scény bludiste.tscn a duch nato kašle jak dveře jako vypadaj
	if not rezim==RezimPohybu.DOMU and not rezim==RezimPohybu.VYPOUSTENI:
		if smer==NAHORU and self.logicka_pozice==Vector2(10,12):
			return false
		elif smer==DOLU and self.logicka_pozice==Vector2(10,10):
			return false

	
	#zavoláme metodu muzu_jit() rodičovský třídy Figurky
	return .muzu_jit(smer)


# funkce co vybere náhodnej směr kterým muže duch jít jakoby ze svý současný pozice
# duch se nesmí vracet noa procházet vokrajema
func nahodny_smer():
	
	# nejdřiv naplníme pole možnejma směrama kterejma byse jako dalo jít
	var smery=[]
	
	if predchozi_smer!=LEVA and muzu_jit(PRAVA):
		smery.append(PRAVA)
	if predchozi_smer!=PRAVA and muzu_jit(LEVA):
		smery.append(LEVA)
	if predchozi_smer!=DOLU and muzu_jit(NAHORU):
		smery.append(NAHORU)
	if predchozi_smer!=NAHORU and muzu_jit(DOLU):
		smery.append(DOLU)
	
	# a vrátíme z pole náhodnej prvek
	return smery[generatorNahodnychCisel.randi() % smery.size()] 
	
#overriddnutej callback tweenu volanýho když skončí krok figurky
#budem v něm dělat duchovu logiku
func _on_TweenKroku_tween_completed(objekt,klic):
	#tou tečkou zavoláme tu samou metodu rodičovskýho oběktu
	._on_TweenKroku_tween_completed(objekt,klic)

	#pokud duch vleze do dírovýho 'teleportu' na straně vobrazovky udělá eště jeden
	#krok ve stejným směru a nepřemejšlí kam má jít
	# v takovýp případě nám tween skončí 
	if jde:
		return
		
	var smer=null
	
	# pokud se má duch votočit no tak prostě udělá krok vopačným směrem než předtim 
	# a hotovo jako
	if otoc_se:
		smer = predchozi_smer * -1
		otoc_se=false
		jdi(smer)
		return
	
	# výběr kroku podle režimu ve kterým duch právě jako je
	match rezim:
		RezimPohybu.VYPOUSTENI:
			#přepnem režim a pošlem ducha dál
			if logicka_pozice==VYPOUSTECI_POZICE:
				rezim=pozadovany_rezim_pohybu
				smer=predchozi_smer
			else:
				smer=urci_nejkraci_smer_k_cili()
				
		RezimPohybu.ROZPTYLENI:
			smer=urci_nejkraci_smer_k_cili()
				
		RezimPohybu.PRONASLEDOVANI:
			urci_cil_pronasledovani()
			smer=urci_nejkraci_smer_k_cili()
			
		# kdž je duch jenom takovejma vočima a letí děsně rychle domu
		RezimPohybu.DOMU:
			#noa až duch dorazí domu ho voživíme a pošlem z domečku na jeho rozptylovou pozici
			if logicka_pozice==DOMECEK_POZICE:
				
				#uděláme efekt výbuchu :D
				var instance=VYBUCH_SKRIPT.new(self.position,barva)
				get_parent().add_child(instance)
				
				# vobnovíme normální zhled a dáme vědět herní scéně žeje duch doma
				rezim=RezimPohybu.JE_DOMA
				emit_signal('doma',self)
				normalni_zhled()
				
			smer=urci_nejkraci_smer_k_cili()
			
		#pokud je duchdoma nebo vyděšenej tak se jenak náhodně poflakuje
		RezimPohybu.VYDESENI, RezimPohybu.JE_DOMA:
			smer=nahodny_smer()
			
	#noa nakonec uděláme krok vybraným směrem
	jdi(smer)
			
