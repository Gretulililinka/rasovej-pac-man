extends Node2D



var efekt=null
var casovac=null

var doba_trvani=0

func nastav(pozice,doba_trvani):
	self.position=pozice
	self.doba_trvani=doba_trvani

func _ready():
	

	#vyrobíme si materiál particle efektu
	var material=ParticlesMaterial.new()
	
	# budem je dělat po povrchu čtverečku o hraně 32 pixelů
	material.emission_shape=ParticlesMaterial.EMISSION_SHAPE_BOX
	material.emission_box_extents=Vector3(32,32,0)
	
	# a posílat nahoru
	# gravitace směrem dolu už je tam zapnutá defaultně
	material.direction=Vector3(0,-1,0)
	material.initial_velocity=100
	material.scale=2
	
	# různý ty random jsou vždycky v rosahu od nuly do jedný
	# třeba tady to jako u každýho particlu to vybere náhodný zvětčení
	material.scale_random=0.5
	# vždycky se náhodně u každýho particlu vybere hodnota od scale_random až k jedný
	# a tim se vynásobí maximální velikost v tom atributu .scale
	# vostatní něco_random fungujou stejně
	
	#nastavíme barevnej přechod barvy jednotlivejch částic v průběhu jejich živůtků
	# z textury s namalovaným gradientem
	material.color_ramp=preload('res://obrazky/gradient.png')
	
	efekt=Particles2D.new()
	efekt.amount=32
	efekt.process_material=material
	#particle efekty v godotu boužel neuměj pracovat s AtlasTexture takže tam mužem
	# strčit akorát tak obyč vobrázek
	efekt.texture=preload('res://obrazky/castice.png')
	
	self.add_child(efekt)
	
	
	self.casovac=Timer.new()
	self.casovac.one_shot=true
	self.add_child(casovac)
	self.casovac.connect('timeout',self,'_on_casovac_skoncil')
	self.casovac.start(doba_trvani)

# efekt budeme vypínat jakoby nadvakrát
# godot nemá v particle systemu něco třeba nějakej signál kterým bysme nějak jednoduše
# zistili jestli jako ještě máme na scéně nějaký částice
# proto nejdřiv vypneme vyrábění novejch částic počkáme dobu jejich života nóó a
# pak víme že už žádný nežijou a mužem efekt odstranit :O ;D
func _on_casovac_skoncil():
	if efekt.emitting:
		efekt.emitting=false
		casovac.start(1)
	else:
		self.queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
