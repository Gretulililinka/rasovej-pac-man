extends Node

# něco jako globální statická proměná do který budem vodevšat přidávat
var skore=0

# proměná co bude držet tabulku nejvyšího skóre
# bude to jakoby pole o deseti záznamech noa každej ten záznam bude pole o dvou prvcích
# záznam se bude zkládat z ména hráče a se skóre  
var tabulka_nejvysiho_skore=[]

#výchozí tabulka nejviších skóre. pokud neexistuje soubor s highscore tak ho vytvoříme
# s timdletim vobsahem
const VYCHOZI_TABULKA=[
			['Martin, looter king',99999],
			['Floyd',50000],
			['Chauvinovo koleno',25000],
			['Barack Hussein Obama',10000],
			['Černoušek Bubu',7500],
			['David Černej',5000],
			['Kunta Kynte',2500],
			['Zemanputinslut',1500],
			['Černej pasažér',100],
			['Ty!!!! ;D ;D',1]
			]

# pod linuxem toje ~/.local/share/godot/skore.txt
# ale liší se na různejch platformách. proto tam maj tamto user:// který to jako 
# řeší za nás ;D
const CESTA_K_SOUBORU='user://skore.txt'

func uloz_nejvysi_skore():
	var soubor = File.new()
	soubor.open(CESTA_K_SOUBORU, File.WRITE)
	for zaznam in tabulka_nejvysiho_skore:
		soubor.store_line(zaznam[0])
		soubor.store_line(str(zaznam[1]))
	soubor.close()

# načtem *.txt soubor s highscore
# pokud neexistuje použijem výchozí hodnoty z 'VYCHZI_TABULKA'
func nacti_nejvysi_skore():
	tabulka_nejvysiho_skore=[]
	var soubor = File.new()
	if soubor.file_exists(CESTA_K_SOUBORU):
		soubor.open(CESTA_K_SOUBORU, File.READ)
		for i in range(10):
			var zaznam=[soubor.get_line(),int(soubor.get_line())]
			tabulka_nejvysiho_skore.append(zaznam)
		soubor.close()
		print(tabulka_nejvysiho_skore)
	else:
		#pokud jako soubor neexistuje načtem výchozí danou tabulku skóre
		tabulka_nejvysiho_skore=VYCHOZI_TABULKA
		
#funkce co zistí jestli jako má cenu skóre zapisovat do tabulky
# kouknem jestli je skóre vyší než poslední záznam
func ma_se_skore_pridat():
	return tabulka_nejvysiho_skore[-1][1]<skore
	
func pridej_skore_do_tabulky(meno):
	
	var posunovany=null
	var novy_zaznam=[meno,skore]
	
	#budem postupovat vodzhora tabulkou a až narazíme na záznam s menčím skórem tak ho nahradíme
	# a původní i se zbytkem tabulky postrčíme o pozici dolu
	for i in range(10):
		var zaznam=tabulka_nejvysiho_skore[i]
		# pokud neposouváme tabulku hledáááme.....
		if posunovany == null:
			if skore>zaznam[1]:
				posunovany=tabulka_nejvysiho_skore[i]
				tabulka_nejvysiho_skore[i]=novy_zaznam
				
		#posuneme zbytek tabulky dolu a poslední záznam 'zahodíme'
		else:
			var pomocna=tabulka_nejvysiho_skore[i]
			tabulka_nejvysiho_skore[i]=posunovany
			posunovany=pomocna
